FROM mysql:5.7
RUN apt-get update -y

COPY . .

RUN /bin/bash -c "/usr/bin/mysqld_safe --skip-grant-tables &" && \
  sleep 5 && \
  mysql -u root -e "CREATE DATABASE efp" && \
  mysql -u root efp < efp-china.sql

EXPOSE 3306